function createCard(name, description, pictureUrl, startDate, endDate, locationName) {
    return `
    <p>
      <div class="card">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <h6 class="card-subtitle mb-2 text-muted">${locationName}</h6
          <p class="card-text">${description}</p>
          <div class="card-footer text-muted">
          ${startDate} - ${endDate}
        </div>
      </div>
      </p>
    `;
  }

  window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';

    try {
      const response = await fetch(url);

      if (!response.ok) {
        // Figure out what to do when the response is bad
        console.log("bad response");
        return (` <div class="alert alert-warning alert-dismissible fade show" role="alert">
  <strong>Holy guacamole!</strong> An error has occured, we still care though.
  <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
</div>`);
      } else {
        const data = await response.json();

        let columnIndex = 0;
        const columns = document.querySelectorAll('.col');

        for (let conference of data.conferences) {
          const detailUrl = `http://localhost:8000${conference.href}`;
          const detailResponse = await fetch(detailUrl);
          console.log(detailResponse);

          if (detailResponse.ok) {
            const details = await detailResponse.json();
            const title = details.conference.name;
            const description = details.conference.description;
            const pictureUrl = details.conference.location.picture_url;
            const startDate = details.conference.starts;
            const endDate = details.conference.ends;
            const locationName = details.conference.location.city;
            const html = createCard(title, description, pictureUrl, startDate, endDate, locationName);
            const column = columns[columnIndex];

            columnIndex = (columnIndex + 1) % columns.length;
            column.innerHTML += html;

            // console.log('Col: ', column )
            // column[0].innerHTML += html;

          }
        }

      }
    } catch (e) {
      // Figure out what to do if an error is raised
      console.log("An error has occured");
      return (` <div class="alert alert-warning alert-dismissible fade show" role="alert">
  <strong>Holy guacamole!</strong> An error has occured, we still care though.
  <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
</div>`);
    }

  });
