import AttendeesList from './AttendeesList'
import Nav from './Nav';
import LocationForm from './LocationForm';
import ConferenceForm from './ConferenceForm';
import AttendConferenceForm from './AttendConference';

function App(props) {
  if (props.attendees === undefined) {
    return null;
  }
  return (
    <>
    <Nav />
    <div className="container">
      {/* <ConferenceForm /> */}
      <AttendConferenceForm />
      {/* <AttendeesList attendees={props.attendees} /> */}
      {/* <LocationForm /> */}
    </div>
    </>
  );
}

export default App;
