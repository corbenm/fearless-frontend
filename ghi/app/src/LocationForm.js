import React, { useEffect, useState } from 'react';

function LocationForm( ) {
//   const [location, setLocation] = useState('');
  const [states, setStates] = useState([]);
  const [name, setName] = useState('');
  const [city, setCity] = useState('');
  const [roomCount, setRoomCount] = useState('');
  const [state, setState] = useState('');


    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value);
    }

    const handleCityChange = (event) => {
        const value = event.target.value;
        setCity(value);
    }

    const handleRoomChange = (event) => {
        const value = event.target.value;
        setRoomCount(value);
    }

    const handleStateChange = (event) => {
        const value = event.target.value;
        setState(value);
    }

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};
        data.name = name;
        data.room_count = roomCount;
        data.city = city;
        data.state = state;
        console.log(data);

        const locationUrl = 'http://localhost:8000/api/locations/';
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(data),
            headers: {
            'Content-Type': 'application/json',
            },
        };

  const response = await fetch(locationUrl, fetchConfig);
  if (response.ok) {
    const newLocation = await response.json();
    console.log(newLocation);

    setName('');
    setRoomCount('');
    setCity('');
    setState('')
    // setStates([]);
  }
    }

    const fetchData = async () => {
    const url = 'http://localhost:8000/api/states/';

    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setStates(data.states);
      console.log(states);
    }
  }

  useEffect(() => {
    fetchData();
  }, []);

    return (
    <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new location</h1>
            <form onSubmit={handleSubmit} id="create-location-form">
              <div className="form-floating mb-3">
                <input onChange={handleNameChange} placeholder="Name" value={name} required type="text" name="name" id="name" className="form-control" />
                <label htmlFor="name">Name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleRoomChange} placeholder="Room count" value={roomCount} required type="number" name="room_count" id="room_count" className="form-control" />
                <label htmlFor="room_count">Room count</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleCityChange} placeholder="City" value={city} required type="text" name="city" id="city" className="form-control" />
                <label htmlFor="city">City</label>
              </div>
              <div className="mb-3">
                <select onChange={handleStateChange} value={state} required name="state" id="state" className="form-select">
                  <option value="">Choose a state</option>
                  {states.map(state => {
                    return (
                    <option key={state.abbreviation} value={state.abbreviation}> {state.state} </option>
                    );
                    })};
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    );
}
export default LocationForm;

// import React, {useEffect, useState} from 'react';

// function LocationForm() {
//   const fetchData = async () => {
//     const url = 'http://localhost:8000/api/states/';

//     const response = await fetch(url);

//     if (response.ok) {
//       const data = await response.json();
//     }
//   }

//   useEffect(() => {
//     fetchData();
//   }, []);

//   // return method...
//   return (
//         <div className="row">
//             <div className="offset-3 col-6">
//               <div className="shadow p-4 mt-4">
//                 <h1>Create a new location</h1>
//                 <form id="create-location-form">
//                   <div className="form-floating mb-3">
//                     <input placeholder="Name" required type="text" name="name" id="name" className="form-control" />
//                     <label htmlFor="name">Name</label>
//                   </div>
//                   <div className="form-floating mb-3">
//                     <input placeholder="Room count" required type="number" name="room_count" id="room_count" className="form-control" />
//                     <label htmlFor="room_count">Room count</label>
//                   </div>
//                   <div className="form-floating mb-3">
//                     <input placeholder="City" required type="text" name="city" id="city" className="form-control" />
//                     <label htmlFor="city">City</label>
//                   </div>
//                   <div className="mb-3">
//                     <select required id="state" name="state" className="form-select">
//                       <option value="">Choose a state</option>
//                     </select>
//                   </div>
//                   <button className="btn btn-primary">Create</button>
//                 </form>
//               </div>
//             </div>
//           </div>
//         );
// }
